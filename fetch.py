"""Helper to run the example-desktop-content fetch tool."""


from pathlib import Path
import runpy
import sys

cwd = Path('.').absolute()

sys.path.insert(0, str(cwd.joinpath('src')))

runpy.run_module('exampledesktopcontent.fetcher')

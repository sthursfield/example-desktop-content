from argparse import ArgumentParser
from pathlib import Path
from typing import List
import logging
import os
import sys

import requests
from strictyaml import load, Map, Str, Int, Optional, Seq, YAMLError

from exampledesktopcontent.content import ContentItem
from exampledesktopcontent.personas import Persona

log = logging.getLogger()


def argument_parser():
    parser = ArgumentParser(
        description="Fetch tool for GNOME example-desktop-content module")
    parser.add_argument('--output', type=Path, default=Path("./lfs"),
                        help="Location to save content files")
    parser.add_argument('--debug', dest='debug', action='store_true',
                        help="Enable detailed logging to stderr")
    parser.add_argument('inputs', type=Path, nargs="+",
                        help="Input YAML files defining content")
    return parser


INPUT_YAML_SCHEMA = Map({
    Optional("content"): Seq(ContentItem.YAML_SCHEMA),
    Optional("personas"): Seq(Persona.YAML_SCHEMA),
})


def load_inputs(input_paths) -> (List[ContentItem]):
    content_items = []
    personas = []
    for path in input_paths:
        text = path.read_text()
        try:
            parsed_yaml = load(text, INPUT_YAML_SCHEMA)
        except YAMLError as e:
            raise RuntimeError(f"Could not load `{path}`: {e}")

        if "content" in parsed_yaml:
            for item in parsed_yaml["content"].data:
                content_items.append(ContentItem(**item))

    return content_items


def file_size(path: Path) -> int:
    return os.path.getsize(path)


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    else:
        logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    content_items = load_inputs(args.inputs)

    output_dir = args.output
    output_dir.mkdir(exist_ok=True)
    for item in content_items:
        path = output_dir.joinpath(item.name)

        # Avoid malicious paths escaping the expected directory tree
        assert path.is_relative_to(output_dir)

        if not path.exists():
            log.info(f"Fetching {item.fetch_url}")
            r = requests.get(item.fetch_url)
            r.raise_for_status()
            path.parent.mkdir(exist_ok=True, parents=True)
            path.write_bytes(r.content)


try:
    main()
except RuntimeError as e:
    sys.stderr.write("ERROR: {}\n".format(e))
    sys.exit(1)

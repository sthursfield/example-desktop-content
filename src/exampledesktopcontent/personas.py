from dataclasses import dataclass, field
from typing import Dict, List

from strictyaml import load, Map, Str, Int, Optional, Seq, YAMLError

from .content import ContentItem


@dataclass
class Persona:
    YAML_SCHEMA = Map({
        "slug": Str(),
        "description": Str(),
        "content": Seq(Str()),
        Optional("searches"): Seq(
            Map({
                "intent": Str(),
                "query": Str(),
            })
        )
    })

    slug: str
    description: str
    content: List[ContentItem]
    searches: dict = field(default_factory=dict)

    def content_item_globs(self) -> List[str]:
        return self.content

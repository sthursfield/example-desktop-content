from dataclasses import dataclass
from typing import Dict, List

from strictyaml import load, Map, Str, Int, Seq, YAMLError, Optional


@dataclass
class ContentItem:
    """One item of example content."""

    name: str
    fetch_url: str
    origin: str
    license: str
    attribution: str = None
    comment: str = None

    YAML_SCHEMA = Map({
        "name": Str(),
        "fetch_url": Str(),
        "origin": Str(),
        "license": Str(),
        Optional("attribution", default=None): Str(),
        Optional("comment", default=None): Str(),
    })

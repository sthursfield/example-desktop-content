from argparse import ArgumentParser
from pathlib import Path
from typing import List
import logging
import os
import sys

import requests
from strictyaml import load, Map, Str, Int, Optional, Seq, YAMLError

from exampledesktopcontent.content import ContentItem
from exampledesktopcontent.personas import Persona
from .output import OutputArchive

log = logging.getLogger()


def argument_parser():
    parser = ArgumentParser(
        description="Build tool for GNOME example-desktop-content module")
    parser.add_argument('--content', type=Path, default=Path("./lfs"),
                        help="Location of content files")
    parser.add_argument('--debug', dest='debug', action='store_true',
                        help="Enable detailed logging to stderr")
    parser.add_argument('--output', type=Path, default=Path("./example-desktop-content.tar.xz"),
                        help="Location for output file")
    parser.add_argument('inputs', type=Path, nargs="+",
                        help="Input YAML files defining content and personas")
    return parser


INPUT_YAML_SCHEMA = Map({
    Optional("content"): Seq(ContentItem.YAML_SCHEMA),
    Optional("personas"): Seq(Persona.YAML_SCHEMA),
})


def load_inputs(input_paths) -> (List[ContentItem], List[Persona]):
    content_items = []
    personas = []
    for path in input_paths:
        text = path.read_text()
        try:
            parsed_yaml = load(text, INPUT_YAML_SCHEMA)
        except YAMLError as e:
            raise RuntimeError(f"Could not load `{path}`: {e}")

        if "content" in parsed_yaml:
            for item in parsed_yaml["content"].data:
                content_items.append(ContentItem(**item))

        if "personas" in parsed_yaml:
            for persona in parsed_yaml["personas"].data:
                personas.append(Persona(**persona))

    return content_items, personas


def file_size(path: Path) -> int:
    return os.path.getsize(path)


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    else:
        logging.basicConfig(stream=sys.stderr, level=logging.INFO)

    content_items, personas = load_inputs(args.inputs)

    archive = OutputArchive(args.output)
    archive.create(args.content, personas=personas)
    size = file_size(args.output)
    log.info(f"Created {archive}, compressed size: {size} bytes")


try:
    main()
except RuntimeError as e:
    sys.stderr.write("ERROR: {}\n".format(e))
    sys.exit(1)

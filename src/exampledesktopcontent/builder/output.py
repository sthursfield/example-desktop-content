from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List
import logging
import tarfile

from strictyaml import load, Map, Str, Int, Seq, YAMLError

from exampledesktopcontent.personas import Persona

log = logging.getLogger(__name__)


def _get_tarfile_write_mode(filename: Path):
    if filename.suffixes == [".tar"]:
        return "w"
    if filename.suffixes == [".tar", ".gz"]:
        return "w:gz"
    if filename.suffixes == [".tar", ".bz2"]:
        return "w:bz2"
    if filename.suffixes == [".tar", ".xz"]:
        return "w:xz"
    raise RuntimeError(
        "Unsupported archive extension. Supported types: `.tar`, `.tar.gz`, "
        "`.tar.bz2`, `.tar.xz`"
    )


@dataclass
class OutputArchive:
    path: str

    def create(self, content_path: Path, personas: List[Persona]):
        log.info("Creating %s", self.path)
        content_item_globs = set()
        for p in personas:
            content_item_globs.update(p.content_item_globs())

        content_item_paths = set()
        for pattern in content_item_globs:
            content_item_paths.update(content_path.glob(pattern))

        mode = _get_tarfile_write_mode(self.path)
        with tarfile.open(self.path, mode=mode) as tf:
            for content_item_path in sorted(content_item_paths):
                log.debug("Adding %s to archive", content_item_path)
                tf.add(
                    content_item_path,
                    arcname=content_item_path.relative_to(content_path),
                )

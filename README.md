# Example Desktop Content

This is a repository of test content, for use when testing content applications and desktop search functionality.

Goals:

  * provide content that we could realistically expect to find on someone's laptop / desktop PC

Non goals:

  * be a "stress test" - we may get to this later, but it's not currently a goal.

Requirements:

  * content sources must be recorded in this repo
  * all content must be freely redistributable
  * any generated content should be reproducible, i.e. document how it was generated
  * content must be grouped to a specific persona and should have some sense within the context of that persona.
  * content must have the smallest possible size. Remember that in most cases we care about the metadata, not the actual content. Compressed formats must always be used.

## How to use

To build the content collection locally:

    python3 ./build.py content/* personas/*

## Finding content

### Music

https://freemusicarchive.org/ is one place to look.

### Pictures

[Flickr](https://flickr.com) is a good source of photos. Things to check:

  * The license is Creative Commons
  * There is EXIF metadata present - this makes the photo a more useful testcase.

[Artvee](https://artvee.com/) has public domain images of classical art.

## Compressing content

### Music

As of 2023-02-27, nothing in GNOME processes the actual audio data unless actually playing the audio.
So consider replacing the audio data with a sinewave or something else that compresses well.

### Photos

As of 2023-02-27, nothing in GNOME processes the actual image data unless they are viewing the
photos. So consider replacing the image data with something that compresses well like a white rectangle.
